# Predict HIV Progression

Open sourced HIV data from Kaggle competition
This contest focuses on using the nucleotide sequence of the Reverse Transcriptase (RT) and Protease (PR) to predict the patient's short-term progression.

These sequences are from patients who had only recently contracted HIV-1 and had not been treated before.

The Dataset is organized as follows:
Col-1: patient ID

Col-2: responder status ("1" for patients who improved and "0" otherwise)

Col-3: Protease nucleotide sequence (if available)

Col-4: Reverse Transciptase nucleotide sequence (if available)

Col-5: viral load at the beginning of therapy (log-10 units)

Col-6: CD4 count at the beginning of therapy

The Responder status indicates whether the patient improved after 16 weeks of therapy.  Improvement is defined as a 100-fold decrease in the HIV-1 viral load.

There's a brief description of Protease nucleotide sequence, Reverse Transciptase nucleotide sequence, viral load and CD4 count on the background page. 

training_data.csv is the training dataset used to calibrate your model. 

test_data.csv is the test datset used to generate submissions.These sequences are from patients who had only recently contracted HIV-1 and had not been treated before.

The Dataset is organized as follows:
Col-1: patient ID

Col-2: responder status ("1" for patients who improved and "0" otherwise)

Col-3: Protease nucleotide sequence (if available)

Col-4: Reverse Transciptase nucleotide sequence (if available)

Col-5: viral load at the beginning of therapy (log-10 units)

Col-6: CD4 count at the beginning of therapy

The Responder status indicates whether the patient improved after 16 weeks of therapy.  Improvement is defined as a 100-fold decrease in the HIV-1 viral load.

There's a brief description of Protease nucleotide sequence, Reverse Transciptase nucleotide sequence, viral load and CD4 count on the background page. 

training_data.csv is the training dataset used to calibrate your model. 

test_data.csv is the test datset used to generate submissions.
